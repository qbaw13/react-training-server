const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
  res.send([
    { label: 'Tomato', type: 'TOMATO' },
    { label: 'Bacon', type: 'BACON' },
    { label: 'Chesse', type: 'CHEESE' },
    { label: 'Meat', type: 'MEAT' },
    { label: 'Eggs', type: 'EGGS' },
  ])
})

module.exports = router;