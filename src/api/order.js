const express = require('express');
const router = express.Router();

router.post('/', (req, res) => {
  validateRequest(req.body, (error, info) => {
    if (error) {
      res.status(400).send({ message: error });
    }
    res.status(200).send(info);
  })
});

const validateRequest = (requestBody, callback) => {
  let info;
  let error;

  if (!requestBody.customer) {
    error = `Missing 'customer' object`;
  }
  else {
    if (!requestBody.customer.name || requestBody.customer.name === '') {
      error = `Missing 'customer.name' value`;
    }
    if (!requestBody.customer.address || requestBody.customer.address === '') {
      error = `Missing 'customer.address' value`;
    }
    if (!requestBody.customer.phone || requestBody.customer.phone === '') {
      error = `Missing 'customer.phone' value`;
    }
  }

  if(!requestBody.order) {
    error = `Missing 'order' object`;
  }
  if(requestBody.order && Object.keys(requestBody.order).length === 0) {
    error = `Empty 'order' object`;
  }

  if(!error) {
    info = { message: 'Order has been sent successfully.'}
  }

  callback(error, info);
};

module.exports = router;