const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const ingredients = require('./api/ingredients');
const order = require('./api/order');

const app = express();
app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(cors());

app.use('/ingredients', ingredients);
app.use('/order', order);

app.listen(process.env.PORT || 3100);